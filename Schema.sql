-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 04, 2017 at 10:13 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_post`
--

CREATE TABLE `blog_post` (
  `USERID` int(20) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `POST_DATE` date NOT NULL,
  `TITLE` varchar(20) NOT NULL,
  `POSTS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_post`
--

INSERT INTO `blog_post` (`USERID`, `POST_ID`, `POST_DATE`, `TITLE`, `POSTS`) VALUES
(6, 7, '2017-09-04', 'test 9/4', ' Test update'),
(6, 8, '2017-09-04', '2nd test 9/4', 'Test 2 for blog entry.'),
(6, 9, '2017-09-04', 'Testing nl2br', 'test\r\nnl2br\r\nworks');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `TITLE` varchar(20) NOT NULL,
  `COM_USER` varchar(20) NOT NULL,
  `ID` int(11) NOT NULL,
  `COMM_ID` int(11) NOT NULL,
  `COM_DATE` date NOT NULL,
  `COMMENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`TITLE`, `COM_USER`, `ID`, `COMM_ID`, `COM_DATE`, `COMMENT`) VALUES
('test 9/4', 'mpaschall', 6, 4, '2017-09-04', 'my test comment'),
('test 9/4', 'mpaschall', 6, 6, '2017-09-04', '2nd comment test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `FIRST_NAME` varchar(20) NOT NULL,
  `LAST_NAME` varchar(20) NOT NULL,
  `E_MAIL` varchar(40) NOT NULL,
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `TYPE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `E_MAIL`, `USERNAME`, `PASSWORD`, `TYPE`) VALUES
(4, 'Administrator', 'Account', 'admin@clc.com', 'admin', '1', 1),
(6, 'Mathew', 'Paschall', 'xdarkzero89@hotmail.com', 'mpaschall', '1234', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_post`
--
ALTER TABLE `blog_post`
  ADD PRIMARY KEY (`POST_ID`),
  ADD KEY `USERID` (`USERID`),
  ADD KEY `TITLE` (`TITLE`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`COMM_ID`),
  ADD KEY `ID` (`ID`),
  ADD KEY `TITLE` (`TITLE`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_post`
--
ALTER TABLE `blog_post`
  MODIFY `POST_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `COMM_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`TITLE`) REFERENCES `blog_post` (`TITLE`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `anID` FOREIGN KEY (`ID`) REFERENCES `users` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
