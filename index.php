<?php/* Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Main landing page, updated to show blogs from today  */
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CLC Project</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Purple Group Blog</h1>
</head>
<body>
<!--splits portions of the site-->
<hr>
    <a class = "option" href= 'Login.php'>Login</a>
    <a class = "option" href= 'Registration.php'>Register</a>
<hr>
<h2 align="center"><b>Blogs</b></h2>
<!--create table-->
<table class="blogs">
    <th>All Blogs</th>
<?php
include 'myfuncs.php';

$conn = dbConnect();
$index = 0;

$user = getUserID();
$date = "20" . date("y/m/d");
//select date, title, and post where date = today
$sql = ("SELECT blog_post.POST_DATE, blog_post.TITLE, blog_post.POSTS, blog_post.USERID, users.ID, users.USERNAME 
FROM blog_post
INNER JOIN users
ON users.ID = blog_post.USERID");
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        //enter result in table
        $blogUserArray[$index] = $row['USERNAME'];
        $blogDArray[$index] = $row['POST_DATE'];
        $blogArray[$index] = $row['TITLE'];
        $blogCArray[$index] = $row['POSTS'];
        $index++;
    }
}
else if($result->num_rows < 1) {
    include('noPosts.php');

}
else{

}

$conn->close();
?>

    <?php
    for ($i = 0; $i < count($blogArray); $i++) {
        $aUser = $blogUserArray[$i];
        $blog = $blogArray[$i];
        $blogContent = $blogCArray[$i];
        $bDate = $blogDArray[$i];
        echo "<tr>" . "<td>" . "<b>" .  $bDate . " " . $aUser .  "</b>" . "</td>" . "</tr>" . PHP_EOL;
        echo "<td>" . $blog . "</td>" . PHP_EOL;
        echo "<tr>" . "<td>" . $blogContent . "</td>" . "</tr>";
        echo "<tr>" . "<td>" . " " . "</td>". "</tr>" . PHP_EOL;
        echo "<tr>" . "<td>" . " " . "</td>". "</tr>" . PHP_EOL;
    }
    ?>
</table>
</body>
</html>