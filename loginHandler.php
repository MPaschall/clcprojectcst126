<?php
/* Purple Group Blog Version 1, Module 4, version 1, Mathew Paschall 8/20/17
Handles login credentials and checking database  */
include 'myfuncs.php';



$conn = dbConnect();

$user = $_POST['userName'];
$pass = $_POST['passWord'];


//Check if username or password is null or blank
if (strlen($user) > 20 || strlen($pass) > 20 )
{
loginLong();
}
else {
    //Select all from users
    $sql = "SELECT * FROM users WHERE USERNAME = '$user' AND PASSWORD= '$pass'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0 && $result->num_rows < 2) {
        while ($row = $result->fetch_assoc()) {
            //save user id and type for login handler
            saveUserId($row['ID']);
            saveUserType($row['TYPE']);
            //check type
            if($row['TYPE'] == 0) {
                include('blogMain.php');
            }
            else if($row['TYPE'] == 1){
                include 'adminMain.php';
            }
        }
    }
    //check for multiple logins
    else if($result->num_rows > 1) {
        multiplLogin();
    }
    //check for incorrect info
    else if($result->num_rows < 1) {
        include('loginFailed.php');
    }

}


$conn->close();
?>