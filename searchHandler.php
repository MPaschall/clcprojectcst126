<?php/* Purple Group Blog Version 1, Module 5, version 1, Mathew Paschall 8/27/17
Handles user search for blogs  */
?>
<html>
<head>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <!--Sets link to previous page-->
    <h1 align="center">Blog Posts</h1>
</head>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='blogMain.php'>Blog Main</a></div>
<hr>
<body>
<table class="blogs">
<?php
include 'myfuncs.php';

$conn = dbConnect();
$user = getUserID();
$title= $_POST["searchable"];
$searchResult = array();
$searchDate = array();
$searchUser = array();
$searchTitles = array();
$searchContent = array();

$searchResult = getBlogByTitle($conn, $title, $user);
$searchDate = $searchResult[0];
$searchUser = $searchResult[1];
$searchTitles = $searchResult[2];
$searchContent = $searchResult[3];

"<th>" . "blogs" . "</th>";
for ($i = 0; $i < count($searchTitles); $i++) {
    $myDate= $searchDate[$i];
    $myUser= $searchUser[$i];
    $myTitle= $searchTitles[$i];
    $myContent = $searchContent[$i];
    echo "<tr>" . "<td>" . '<a href="searchedBlog.php?bTitle= ' . $myTitle . '&bContent= ' . $myContent . '"> '
        . $myDate . " " . $myUser . " " . $myTitle . " " . '</a>' . "</td>" . "</tr>";
}
?>
</table>
</body>
</html>
