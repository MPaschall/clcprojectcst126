<html>
<!--Purple Group Blog Version 1, Module 5, version 1, Mathew Paschall created 8/27/17
shows search results -->
<?php $blog = $_GET['bTitle'];
$blogContent = $_GET['bContent'];
include 'myfuncs.php';
$conn = dbConnect();
?>
<head>
    <title>Searched Blog</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Purple Group Blog</h1>
</head>
<body>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='blogMain.php'>Blog Main</a></div>
<?php
echo "<tr>" . "<td>" . '<a class = "option" href="blogComment.php?commTitle= ' . $blog . '"> ' .
    "Comment" . '</a>' . "</td>" . "</tr>";
?>
<hr>
<table class="blogs">
    <?php
    echo "<th>" . $blog . "</th>";
    echo "<tr>" . "<td>" . $blogContent . "</td>" . "</tr>";

    ?>
</table>
<table class="blogs">
    <th>Comments</th>
    <br>
    <?php
    $resultArray = getComments($conn, $blog);
    $commUser = $resultArray[0];
    $commDate = $resultArray[1];
    $commMsg = $resultArray[2];

    for ($i = 0; $i < count($commDate); $i++) {
        $cUser = $commUser[$i];
        $cDate = $commDate[$i];
        $cMsg = $commMsg[$i];
        echo "<tr>" . "<td>" . " " . $cDate . " " . $cUser . "</td>" . "</tr>";
        echo "<td>" . $cMsg . "</td>";
    }
    ?>
</body>
</html>