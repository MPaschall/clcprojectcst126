<!--Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Allows user to login-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <h1 align="center">Login</h1>
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>

</head>
<hr>
<a class = "option" href='index.php'>Main Menu </a>
<hr>
<body>
<!--Action when submit button is pressed, creates labels and textboxes-->
<form action="loginHandler.php" method="POST">
    <div class="center">
        <label><b>Username</b></label>
            <input type="text" placeholder="Required" name="userName" required>
        <br>
        <label><b>Password</b></label>
            <input type="text" placeholder="Required" name="passWord" required>
        <br>
        <br>
        <button type="Login" class="logButton">Login</button>
    </div>

</form>
</body>
</html>