<?php /* Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Main screen for selecting to create or view blogs  */
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog Main</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Purple Group Blog Main</h1>
</head>
<hr>
<a class = "option" href="Logout.php">Logout</a>
<hr>
<body>
<!--Sets link positions-->
<div class="blogOption">
    <a href='search.php'>Search Blogs</a>
    <a href='otherBlogs.php'>Other Blogs</a>
    <a href='blogEntry.php'>New Blog</a>
    <a href='viewBlog.php'>View My Blog</a>
</div>
</body>
</html>