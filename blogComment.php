<!-- Purple Group Blog Version 1, Module 6, version 1, Mathew Paschall 9/3/17
Allows users to comment on posts  -->

<html>
<?php
$myTitle = $_GET['commTitle'];
?>
<head>
    <title>Blog Comment</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Update Blog</h1>
</head>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='search.php'>Search Blogs</a>
<hr>
<body>
<body>
<table class="blogs">
    <!--Action when submit button is pressed, creates labels and textboxes-->
    <form action="commentHandler.php" method="POST">
        <div class="center">
            <th><input type="text" name= "titleComm" value= "<?php echo trim($myTitle); ?>" readonly></th>
            <tr><td><textarea name="commEntry" placeholder="My Comment(<50 chars)" rows= "10" cols= "30"></textarea></td></tr>
            <br>
            <br>
            <tr><td><button type="Submit" class="logButton">Update</button></td></tr>
        </div>
    </form>
</table>
</body>
</html>
</body>
</html>