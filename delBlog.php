<html>
<!-- Purple Group Blog Module 6, version 3, Mathew Paschall 9/3/17
Allows users to Delete posts  -->
<?php
$myTitle = $_GET['delTitle'];
$myContent = $_GET['delContent'];
?>
<head>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center"><b>Delete Blog</b></h1>
</head>
<body>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href= "viewBlog.php">View Blogs</a></div>
<hr>
<!--Action when submit button is pressed, creates labels and textboxes-->
<div class="center">
    <table class="blogs">
        <form action="deleteBlog.php" method="POST">
            <div class="center">
            <th><input type="text" value="<?php echo trim($myTitle);?>" name="delTitle"></th>
            <tr><td><textarea name="entryUpd"  rows= "10" cols= "30"><?php echo trim($myContent); ?> </textarea></td></tr>
            <br>
            <br>
            <tr><td><button type="Submit"  onclick="return confirm('Are you sure?')" class="logButton">Delete</button></td></tr>
            </div>
        </form>
    </table>
</div>
<br>
</body>
</html>