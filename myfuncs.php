<?php
/* Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Contains reusable functions for the program  */

//Connect to DB
function dbConnect(){
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $dbname = "login_db";

    $conn = mysqli_connect($servername, $username, $password, $dbname);
    return $conn;
}

//Save user session
function saveUserId($id)
{
    session_start();
    $_SESSION[“USER_ID”] = $id;
}
//Save user type
function saveUserType($type)
{
    session_start();
    $_SESSION[“USER_TYPE”] = $type;
}

//Get user type
function getUserType()
{
    session_start();
    return $_SESSION[“USER_TYPE”];
}

//Get user ID
function getUserId()
{
    session_start();
    return $_SESSION[“USER_ID”];
}

//Check registration info
function checkReg($first, $last, $email, $user, $pass){
    if ($first == NULL || $first == "")
    {
        echo "The First Name or is a required field and cannot be blank.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else if ($last == NULL || $last == "")
    {
        echo "The Last Name or is a required field and cannot be blank";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else if ($email == NULL || $email == ""){
        echo "the email is required and cannot be blank.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else if ($user == NULL || $user == "")
    {
        echo "The username is a required field and cannot be blank";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else if ($pass == NULL || $pass == "")
    {
        echo "The password or is a required field and cannot be blank";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else if(strlen($first) > 20 || strlen($last) > 20 || strlen($email) > 40 || strlen($user) > 20 || strlen($pass) > 20){
        echo "Information too long, please enter values in the proper length.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "Registration.php">' . "Continue" . '</a>';
        return $true;
    }
    else{
        $true = '1';
        return $true;
    }

}

//check blog entry
function checkBlogEntry($atitle, $entry){
    if ($atitle == NULL || $atitle == "")
    {
        echo "Title is required.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else if ($entry == NULL || $entry == "")
    {
        echo "Blog entry required.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else if(strlen($entry) > 100){
        echo "Entry too long, try again";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else{
        $true = '1';
        return $true;
    }
}

function checkComm($entry){
   if ($entry == NULL || $entry == "")
    {
        echo "Comment entry required.";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else if(strlen($entry) > 50){
        echo "Entry too long, try again";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else{
        $true = '1';
        return $true;
    }
}

function commSuccess(){
    $success = "Comment added successfully";
    echo " " . $success;
    echo "<br>";
    echo "<br>";
    echo '<a href= "blogMain.php">' . "Continue" . '</a>';
}

//check title entry
function checkTitle($delTitle){
    if($delTitle == NULL || $delTitle == ""){
        $true = 0;
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else if(strlen($delTitle) > 20){
        echo "Title too long, try again";
        $true='0';
        echo "<br>";
        echo "<br>";
        echo '<a href= "blogMain.php">' . "Continue" . '</a>';
        return $true;
    }
    else{
        $true = 1;
        return $true;
    }
}

//blog success
function blogSuccess(){
    $success= "Blog successfully saved.";
    echo " " . $success;
    echo "<br>";
    echo "<br>";
    echo '<a href= "blogMain.php">' . "Continue" . '</a>';
}

function loginLong(){
    $long = "login or password is too long, try again";
    echo " " . $long;
    echo "<br>";
    echo "<br>";
    echo '<a href= "Login.php">' . "Login" . '</a>';
}

//blog deletion error
function blogDelFail(){
    $unsuccessful= "Blog not found, or you don't have permission to delete this blog.";
    echo " " . $unsuccessful;
    echo "<br>";
    echo "<br>";
    echo '<a href= "blogMain.php">' . "Blog Main" . '</a>';
}

function blogDelSuccess(){
    $unsuccessful= "Blog deleted successfully.";
    echo " " . $unsuccessful;
    echo "<br>";
    echo "<br>";
    echo '<a href= "blogMain.php">' . "Blog Main" . '</a>';
}

//fail to update blog
function updateFail(){
    $unsuccessful= "Error, blog not updated.";
    echo " " . $unsuccessful;
    echo "<br>";
    echo "<br>";
    echo '<a href= "blogMain.php">' . "Blog Main" . '</a>';
}

//successful registration
function regSuccess(){
    $success= "Registered successfully.";
    echo " " . $success;
    echo "<br>";
    echo "<br>";
    echo '<a href= "index.php">' . "Main Menu" . '</a>';
}

//registration failure
function regFailure(){
    $unsuccessful= "Error, try registration again.";
    echo " " . $unsuccessful;
    echo "<br>";
    echo "<br>";
    echo '<a href= "Registration.php">' . "Try again" . '</a>';
}

//multiple logins error
function multipleLogin(){
    $multiple= "There is already a user with that name. Please try again";
    echo " " . $multiple;
}
//user already registered error
function alreadyRegistered(){
    $registered = "user already registered";
    echo $registered;
    echo "<br>";
    echo "<br>";
    echo '<a href= "Registration.php">' . "Try again" . '</a>';
}
//search by title
function getBlogByTitle($conn, $title, $user)
{
    $thisTitle = $title;
    $sql = "SELECT blog_post.POST_DATE, blog_post.TITLE, blog_post.POSTS, blog_post.USERID, users.ID, users.USERNAME 
    FROM blog_post
    INNER JOIN users
    ON users.ID = blog_post.USERID
    WHERE blog_post.USERID!= '$user' AND TITLE LIKE '%$thisTitle%'";
    $result = $conn->query($sql);
//create arrays
    $blogArray = array();
    $blogDArray = array();
    $blogUArray = array();

    $index = 0;

    if ($result->num_rows > 0) {
        //add data to arrays
        while ($row = $result->fetch_assoc()) {
            $blogDArray[$index] = $row['POST_DATE'];
            $blogUArray[$index] = $row['USERNAME'];
            $blogArray[$index] = $row['TITLE'];
            $blogCArray[$index] = $row['POSTS'];
            $index++;

        }
        return array($blogDArray, $blogUArray, $blogArray, $blogCArray);
    }
    else if ($result->num_rows < 1) {
        include('noPosts.php');

    }
    else {
        return Null;
    }

    $conn->close();

}


function getComments($conn, $title)
{
    $atitle = trim($title);
    $sql = "SELECT COM_USER, COM_DATE, COMMENT FROM comments WHERE TITLE= '$atitle'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $dateArray = array();
        $userArray = array();
        $commentArray = array();

        $index = 0;
        //add data to arrays
        while ($row = $result->fetch_assoc()) {
            $dateArray[$index] = $row['COM_USER'];
            $userArray[$index] = $row['COM_DATE'];
            $commentArray[$index] = $row['COMMENT'];
            $index++;
        }
        return array($userArray, $dateArray, $commentArray);
    } else {
        return NULL;
    }
}

