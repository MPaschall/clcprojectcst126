<?php /* Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Main screen for selecting to create or view blogs  */
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <!--Sets style of page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--set logo position-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
</head>
<!--Sets heading centered on the page-->
<h1 align="center">Purple Group Blog Main</h1>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class= "option" href='index.php'>Main Menu </a></div>
<hr>
<body>
<!--Centers links on the page-->
<div class="center">
    <a href='getAllUsers.php'>View Users</a>
    <a href='viewBlog.php'>View Blogs</a>
</div>
</body>
</hr>
</html>