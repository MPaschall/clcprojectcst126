<?php
/*Purple Group Blog Version 1, Module 4, version 1, Mathew Paschall 8/20/17
Handles logging out of the system*/
//logout
    session_start();
    unset($_SESSION[“USER_ID”]);
    header("Location: index.php");

?>