<!-- Purple Group Blog Version 1, Module 4, version 3, Mathew Paschall 9/4/17
Allows users to update their posts  -->

<html>
<?php
$myTitle = $_GET['updTitle'];
$myContent = $_GET['updContent'];
?>
<head>
    <title>Update Blog</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Update Blog</h1>
</head>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='viewBlog.php'>View Blogs</a>
<hr>
<body>
<body>
<table class="blogs">
    <!--Action when submit button is pressed, creates labels and textboxes-->
    <form action="update.php" method="POST">
        <div class="center">
            <th><input type="text" name= "titleUpd" value= "<?php echo trim($myTitle); ?>" ></th>

            <tr><td><textarea name="entryUpd"  rows= "10" cols= "30"><?php echo nl2br(trim($myContent)); ?> </textarea></td></tr>
            <br>
            <br>
            <tr><td><button type="Submit" class="logButton">Update</button></td></tr>
        </div>
    </form>
</table>
</body>
</html>
</body>
</html>