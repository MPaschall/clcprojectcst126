<!--Purple Group Blog Version 1, Module 4, version 2, Mathew Paschall 8/20/17
Allows registration for new users-->


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>"Registration Form"</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>

</head>
<h1 align="center">Register</h1>
<hr>
<a class = "option" href='index.php'>Main Menu </a></div>
<hr>
<body>

<!--Action when submit button is pressed, creates labels and textboxes-->
<form action="registerHandler.php" method="POST">

    <br>
    <br>
    <div class="centered">
        <label><b>First</b></label>
            <input type="text" placeholder="First(<20 chars)" name="firstName" required>
        <br>
        <label><b>Last</b></label>
            <input type="text" placeholder="Last(<20 chars)" name="lastName" required>
        <br>
        <br>
        <label><b>Email</b></label>
            <input type="text" placeholder="Email(<40 chars)" name="eMail" required>
        <br>
        <br>
        <label><b>Username</b></label>
            <input type="text" placeholder="Username(<20 chars)" name="userName" required>
        <br>
        <label><b>Password</b></label>
            <input type="text" placeholder="Password(<20 chars)" name="passWord" required>
        <br>
        <br>
        <button type="Register" class="regButton">Register</button>
    </div>

</form>
</body>
</html>