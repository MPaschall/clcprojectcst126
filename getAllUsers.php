<?php/* Purple Group Blog Version 1, Module 4, version 1, Mathew Paschall 8/20/17
Gets all users for admin */
?>

<html>
<head>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
</head>
<body>
<h1 align="center"><b>Users</b></h1>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='adminMain.php'>Admin Main</a>
<hr>
<!--new table with 3 headings-->
<table style="margin-left: 550px; margin-top: 75px; " border="1">
    <th>ID</th>
    <th>Username</th>
    <th>Password</th>

<?php
include 'myfuncs.php';

$conn = dbConnect();

//select all from users
$sql = "SELECT * FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        //display results in table
        echo  "<tr>" . "<td>" . $row['ID'] . "</td>" . "<td> " . $row['USERNAME'] . "</td>" . "<td>" . $row['PASSWORD']
            . "</td>" . "</tr>";

    }
    }
    else{
    echo "empty table";
    }

$conn->close();
?>
</table>
</body>
</html>
