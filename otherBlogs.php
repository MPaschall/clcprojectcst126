<?php/* Purple Group Blog Version 1, Module 4, version 3, Mathew Paschall
updated 8/27/17 to allow title selection
Allows users to see their blog posts  */
?>
<html>
<head>
    <h1 align="center">Blog Posts</h1>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
</head>
<body>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='blogMain.php'>Blog Main</a>

<table class="blogs">
    <th>Other Blogs</th>

    <?php
    include 'myfuncs.php';

    $conn = dbConnect();

    $user = getUserID();
    $index=0;
    $blogArray = array();
    $blogDArray = array();
    $blogUArray = array();

    $type = getUserType();

    //select date, title, and posts where id = logged in user
    if($type == 0) {
        $sql = ("SELECT blog_post.POST_DATE, blog_post.TITLE, blog_post.POSTS, blog_post.USERID, users.ID, users.USERNAME 
FROM blog_post
INNER JOIN users
ON users.ID = blog_post.USERID
WHERE blog_post.USERID!= '$user'");
        echo '<a class = "option" href="blogMain.php">' . "Blog Main" . '</a>';?>

        <hr>
        <?php
    }
//select all date, title, and posts if admin
    else if($type == 1){
        $sql = ("SELECT blog_post.POST_DATE, blog_post.TITLE, blog_post.POSTS, blog_post.USERID, users.ID, users.USERNAME 
FROM blog_post
INNER JOIN users
ON users.ID = blog_post.USERID");
        echo '<a class = "option" href="adminMain.php">' . "Admin Main" . '</a>';?>
        <hr>
        <?php
    }
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            //creates arrays to use for tables and links
            $blogDArray[$index] = $row['POST_DATE'];
            $blogUArray[$index] = $row['USERNAME'];
            $blogArray[$index] = $row['TITLE'];
            $blogCArray[$index] = $row['POSTS'];
            $index++;

        }

    }
    else if($result->num_rows < 1) {
        include('noPosts.php');

    }
    else{

    }

    $conn->close();
    ?>
    <!--creates table of title links-->
    <?php
    for ($i = 0; $i < count($blogArray); $i++) {
        $blogDate = $blogDArray[$i];
        $blogUser = $blogUArray[$i];
        $blog = $blogArray[$i];
        $blogContent = $blogCArray[$i];
        echo "<tr>" . "<td>" . '<a href="otherBlogSelect.php?bTitle= ' . $blog . '&bContent= ' . $blogContent . '"> '
            . $blogDate . " " . $blogUser . " " . $blog . " " . '</a>' . "</td>" . "</tr>";
    }
    ?>

</table>
</body>
</html>
