<!DOCTYPE html>
<!--Purple Group Blog Version 1, Module 5, version 1, Mathew Paschall 8/27/17
Allows user to search for blogs -->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Search for blogs</h1>
</head>
<body>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href='blogMain.php'>Blog Main</a>
<hr>
<div class="center">
<form action="searchHandler.php" method="POST">
    <label>Blog title</label>
    <br>
    <input type="text" name="searchable" placeholder="required" required>
    <br>
    <br>
    <button type="Submit">Search</button>
</form>
</div>
</body>
</html>