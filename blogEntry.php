<!--Purple Group Blog Version 1, Module 4, version 1, Mathew Paschall 8/20/17
Allows the user to create blogs-->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
</head>
<!--Sets styles for the page-->
<link rel="stylesheet" type="text/css" href="pageStyle.css">
<!--Sets logo and link positions-->
<div class="topright">
    <img src="logo.png" alt="logo" style="width:100px;height:100px;">
</div>
<!--Action when submit button is pressed, creates labels and textboxes-->
<form action="entryHandler.php" method="POST">
    <h1 align="center">Purple Group Blog Main</h1>
    <hr>
    <a class = "option" href= 'Logout.php'>Logout</a>
    <a class = "option" href='blogMain.php'>Blog Main</a></div>
    <hr>
    <body>
    <div class="center">
        <br>
        <br>
        <label><b>Blog Entry</b></label>
        <br>
        <input type="text" placeholder="Title(< 20 chars)" name="title">
        <br>
        <label><b>Message</b></label>
        <br>
        <textarea name="message" placeholder="My Message(< 100 chars)" rows= "10" cols= "30"><?php nl2br();?>
</textarea>
        <br>
        <button type="Submit" class="saveButton">Save</button>

    </div>
    </hr>
    </body>
</form>
</html>