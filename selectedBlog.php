<html>
<!--Purple Group Blog Version 1, Module 5, version 1, Mathew Paschall created 8/27/17
shows selected blog from viewBlog -->
<?php
$blog = $_GET['bTitle'];
$blogContent = $_GET['bContent'];
include 'myfuncs.php';
$type = getUserType();
?>
<head>
    <title>Selected Blog</title>
    <!--Sets styles for the page-->
    <link rel="stylesheet" type="text/css" href="pageStyle.css">
    <!--Sets logo and link positions-->
    <div class="topright">
        <img src="logo.png" alt="logo" style="width:100px;height:100px;">
    </div>
    <h1 align="center">Purple Group Blog</h1>
</head>
<hr>
<a class = "option" href= 'Logout.php'>Logout</a>
<a class = "option" href = 'viewBlog.php' >View Blogs</a >
<?php
echo "<tr>" . "<td>" . '<a class = "option" href="updBlog.php?updTitle= ' . $blog . '&updContent= ' . $blogContent . '"> ' .
    "Update Blog" . '</a>' . "</td>" . "</tr>";
echo "<tr>" . "<td>" . '<a class = "option" href="delBlog.php?delTitle= ' . $blog . '&delContent= ' . $blogContent . '"> ' .
    "Delete Blog" . '</a>' . "</td>" . "</tr>";
    ?>
<hr>
<body>
<table class="blogs">
<?php
$user = getUserId();
echo "<th>" . $blog . "</th>";
echo "<tr>" . "<td>" . $blogContent . "</td>" . "</tr>";

$conn = dbConnect();
$title = trim($blog);
$resultArray = getComments($conn, $title);
$commUser = $resultArray[0];
$commDate = $resultArray[1];
$commMsg = $resultArray[2];

?>
</table>
<table class="blogs">
    <th>Comments</th>
    <br>
    <?php

    for ($i = 0; $i < count($commDate); $i++) {
    $cUser = $commUser[$i];
    $cDate = $commDate[$i];
    $cMsg = $commMsg[$i];
    echo "<tr>" . "<td>" . " " . $cDate . " " . $cUser . "</td>" . "</tr>";
    echo "<td>" . $cMsg . "</td>";
    }
    ?>
</table>
</body>
</html>